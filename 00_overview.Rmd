# Jan 16: Введение {-#overview}

## Запись занятия {-}

Запись занятия 16 января:

<iframe width="560" height="315" src="https://www.youtube.com/embed/BIwYQ3xwCFQ?start=196" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Все записи организованы в [*плейлист*](https://www.youtube.com/playlist?list=PLEwK9wdS5g0rzlOC2O3Oa4nlHpU4ai-FK)



## Data culture {-}

Проект Data Culture направлен на то, чтобы у студентов всех программ бакалавриата Высшей школы экономики появилось понимание возможностей современных технологий в области Data Science.

https://www.hse.ru/dataculture/

<br>

## Обо мне {-}

 - продуктовый аналитик в Pixonic
 
 - продуктовый аналитик в GameInsight
 
 - аналитик в Консультант+
 
 - аспирант СПбГУ (когнитивная психология)

<br>

## Contacts {-}

 - @konhis в [*pmsar2020da.slack.com*](pmsar2020da.slack.com) (основное средство коммуникации)

 - upravitelev@gmail.com (дополнительное средство коммуникации)

 - +7-965-425-5919 (для экстренных случаев)

<br>

## Slack invite link {-}

Все взаимодействие по курсу будем вести в Slack, это мессенджер для команд, очень популярен в IT и среди датасатанистов. Если еще не зарегистрировались, ссылка на инвайт (активна до 25 января):

https://join.slack.com/t/pmsar2020da/shared_invite/zt-ktjr274m-fWTfGCrBaNG52o6mNBZTRw


## Syllabus {-}

 Основы синтаксиса R.
 
 Виды источников данных. Импорт данных из разных типов файлов данныx - SPSS, Excel, *.csv и прочих.
 
 Методы сбора удаленных данных. Скрапинг полуструктурированных сайтов. Изучим XPath и как извлекать данные из html-кода страницы сайтов. Попутно научимся писать циклы и функции.
 
 Удаленные базы данных и API: простейшие API --- что такое, зачем, как читать документацию. Работа с API VK или другого сервиса.
 
 Анализ и представление результатов исследования: много времени будем уделять визуализациям, а также будем работать в Rmarkdown
 
<br>

## Домашнее задание {-}

### Организационное {-}

 - Проверьте и при необходимости отредактируйте профиль в Slack - необходимо указать имя и фамилию (латиницей, в виде `Name Family name`), по возможности - поставьте свою фотографию на аватарку. 

 - Если еще не писали в канале #welcome, то напишите пару слов о себе --- тему своей магистерской (если уже известна), какой опыт работы с R и вообще языками программирования, какие ожидания от курса.

<br>

### R {-}

#### установка R и Rstudio {-}

 - установите язык R (https://cran.r-project.org/)  
 - установите Rstudio (RStudio: https://www.rstudio.com/products/rstudio/download/)

<br>

#### Операторы и создание векторов {-}

Задание необязательное, но настоятельно рекомендую прорешать всем, кто ни разу не сталкивался с R или давно все забыл и не уверен в своих знаниях. Если решение какого-то задания вызвало у вас сомнения, напишите мне об этом в личке в slack.

При работе используйте [*гайд*](http://style.tidyverse.org/index.html) по стилю оформления кода.

Старайтесь, чтобы код был максимально простым. Если каждое упражнение занимает больше одной-двух строк - скорее всего вы что-то делаете не так.

- Создайте объект `x` со значением 5. Создайте объект `y`, который равен `3 * x`. Выведите его на печать.

- Проверьте, что значение выражения `x` - `y` не равно 84.

- Запишите в объект `alarm` результат сравнения, больше ли `x` чем `y`. Выведите объект на печать.

- Создайте вектор `vec` из трех элементов со значениями: `5`, `'a'`, `56`. Выведите на печать.

- Симулируйте выпадение какой-либо грани двенадцатигранного кубика (одно значение).

- Создайте вектор `vec_dice` с результатами десяти бросков восьмигранного кубика (d8).

- Найдите длину меньшего катета прямоугольного треугольника с катетом длиной 4 и гипотенузой длиной 8, и округлите длину до третьего знака. Вам потребуются оператор возведения в степень и функции `sqrt()` и `round()`. На всякий случай напомню, квадрат гипотенузы равен сумме квадратов катетов.
