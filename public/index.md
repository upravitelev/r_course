--- 
title: "Автоматизированный сбор больших данных в экономико-социологических исследованиях"
author: 'Ph.A.Upravitelev'
date: "2022-02-04"
site: bookdown::bookdown_site
---

# Intro {-}

## О курсе {-}

Я (Филипп Управителев) читал курс "Автоматизированный сбор больших данных в экономико-социологических исследованиях" в 2020/21 учебном году на факультете социальных наук НИУ ВШЭ магистрантам-социологам. Курс идёт по выбору и входит в программу Data Culture. Моя основная задача была в том, чтобы сформировать основу для работы с данными, которые нельзя обработать в MS Excel или SPSS: дать основы языка R, научить манипуляциям с таблицами, основным типам визуализаций, импорту данных из разных источников (от файлов до API и полуструктурированных таблиц на сайтах). 

Курс построен по принципу нарастания сложности операций --- мы начинаем с основ языка (операторы, объекты), потом учимся работать с таблицами и делать основные операции с ними, далее приходим к визуализациям и скрапингу / обращениям к API. Каждый последующий блок опирается на изученное ранее, а новый теоретический материал я даю только в тот момент, когда он необходим для конкретной инструментальной задачи. В частности поэтому циклы идут вместе с темой скрапинга данных, а создание собственных функций --- при работе с API.

## R intro {-}

Курс является частью проекта [*R Intro*](https://rintro.ru), который также включает в себя введение в язык R, конспекты по конкретным пакетам, записи вебинаров и прочие материалы. Обновление материалов проекта ведётся по принципу пакетов, то есть с некоторой эпизодичностью и журналом изменений.

## Контакты {-}

Если у вас есть комментарии по логике и материалам курса или, может быть, предложения по темам или методике преподавания языка --- напишите, пожалуйста, мне (\@konhis, Philipp Upravitelev) в [*telegram*](https://t.me/konhis) или в [*ODS*](https://app.slack.com/client/T040HKJE3/D1D8W8QG4).

## Поддержать проект  {-}

Если вам понравились материалы или идея проекта [*R Intro*](https://rintro.ru) и его разделов, вы можете купить мне [**чашечку кофе**](https://yoomoney.ru/to/410018512568186) или порадовать ежемесячной [**подпиской**](https://boosty.to/rintro). Для тех, кто не в России, так же есть [**ko-fi**](https://ko-fi.com/rintro).

## Программа курса {-}

- January 16: [*Вводная лекция*](#overview). [*R intro: операторы и векторы*](#rintro). Арифметические операторы в R, оператор присвоения, логические операторы. Создание векторов.

- January 23: [*Работа с векторами*](#vectors). Извлечение элементов по номеру позиции (индексу) и по логическому условию. Проверка на вхождение значений одного вектора в другой вектор.

- January 30: [*Списки*](#lists). Модификация векторов. Создание списков. Именованные списки. Просмотр структуры объекта. Доступ к значениям по индексу и по названию. 

- February 6: [Таблицы](#dt1). Три фреймворка для работы с таблицами - data.frame, tibble и data.table. Создание data.table. Выбор строк. Операции с колонками. Операции с таблицами (`rbind()`)

- February 13: [Таблицы, pt2](#dt2). Применение функций к колонкам. ifelse. Группировки.

- February 27: [Таблицы, pt3](#dt3). Разбор контрольной. Разбор домашней работы. merge().

- March 6: [*Таблицы, pt4. ETL*](#dt4). Разбор домашней работы. Импорт csv и xls/xlsx-файлов.

- March 13: [*ETL*](#etl). Разбор домашней работы. Пара слов про dcast(). Импорт spss-файлов.

- March 20: [*Visualization*](#dataviz). Инфографика. Типы графиков. Интерактивные графики. Дашборды. Принципы визуализаций. Графики, которые обманывают.

- March 28: [*ggplot2 pt1*](#ggplot2). Введение в логику ggplot2. Grammar of graphics, базовые геомы - точки, линии, текстовые метки. 

<!-- ## HW 1 -->
<!-- [Первая оценочная домашняя работа](HW1.html) -->

- April 24: [*ggplot2 pt2*](#ggplot22). Продолжение по пакету статичной визуализации ggplot2 - статистические геомы (барчарты, гистограммы, боксплоты), линии трендов / сглаживаний. Настройка визуальных параметров графика - осей, цветовых схем, легенды и прочих. [*plotly*](#plotly). Пакет для интерактивных графиков. Основные типы графиков, настройка визуальных параметров и общих параметров

- May 15: [*Web-скрапинг*](#rvest) - основы html, скрапинг данных с помощью пакета rvest, основы xpath.

- May 22: [*Web-скрапинг, циклы и создание функций*](#programming) - разбор предыдущей домашки по скрапингу, начало скрапинга сайта журнала. Интро в циклы и создание функций.

- May 29: [*API*](#api) - доделываем скрапинг сайта журнала. Продолжаем писать функции. Интро в API.

<!-- ## HW 2 -->
<!-- [Вторая оценочная домашняя работа](HW2.html) -->

- June 5: [*VK API*](#vkapi) - изучаем VK API (документация, подключение, особенности сбора данных). Строим граф связей, кто из наших друзей с кем в друзьях.
