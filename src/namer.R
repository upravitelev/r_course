namer::name_dir_chunks("./pages")

# namer::name_chunks("./chapters_pdf/10-visualization.Rmd")


files <- list.files("./pages", pattern = '\\.Rmd$', full.names = TRUE)
lapply(files, namer::unname_all_chunks)


files <- list.files(".", pattern = '\\.Rmd$', full.names = TRUE)
lapply(files, namer::unname_all_chunks)

namer::name_dir_chunks(".")
