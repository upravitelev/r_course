library(rvest)
library(data.table)

# article_url <- 'https://ecsoc.hse.ru/2020-21-2/352980344.html'
article_url <- 'https://ecsoc.hse.ru/2020-21-1/337414467.html'

article_fetcher <- function(article_url) {
  # импортируем страницу
  page <- read_html(article_url)
  
  # выдираем данные со страницы
  path_author <- html_nodes(page, xpath = '//div[@class="centercolumn"]/h3/i') %>% html_text()
  path_author <- paste(path_author, collapse = ',')
  
  path_title <- html_node(page, xpath = '//div[@class="centercolumn"]/h2[@class="article-header"]') %>% html_text()
  path_ann <- html_node(page, xpath = '//div[@class="centercolumn"]/div[@class="annot"]' ) %>% html_text() 
  path_keywords <- html_node(page, xpath = '//div[@class="centercolumn"]/div[@class="keywords"]') %>% html_text()
  
  # собираем все в таблицу
  article_content <- data.table(
    author = path_author,
    title = path_title,
    annotation = path_ann,
    keywords = path_keywords,
    url = article_url
  )
  return(article_content)
}

path <- 
'//*[@id="mainarea"]/div[2]/div[3]'
'//*[@id="mainarea"]/div[2]/div[12]'

# ссылка раздела
'https://ecsoc.hse.ru/rubric/26590246_page=2_sort=author.html'


'https://ecsoc.hse.ru/rubric/26590246_page=1_sort=author.html'
'https://ecsoc.hse.ru/rubric/26590246_page=13_sort=author.html'

# страница рубрики
rubric_url <- 'https://ecsoc.hse.ru/rubric/26590246.html'
rubric <- read_html(rubric_url)

# номер последней подстраницы
last_page <- html_node(rubric, xpath = '//div[@class="pageNav"]/span[@class="last"]/a') %>% html_text()
last_page <- as.numeric(last_page)
last_page

rubric_content <- lapply(1:13, function(subpage_counter) {
  page10_url <- paste('https://ecsoc.hse.ru/rubric/26590246_page=', subpage_counter, '_sort=author.html', sep = '')
  page10 <- read_html(page10_url)
  
  page10_content <- lapply(3:12, function(counter) {
    path <- paste('//*[@id="mainarea"]/div[2]/div[', counter, ']/small/a[2]', sep = '')
    url <- html_node(page10, xpath = path) %>% html_attr('href')

    if (!is.na(url)) {
      article_content <- article_fetcher(article_url = url)
      return(article_content)
    }
  })
  page10_content <- rbindlist(page10_content)
})
rubric_content <- rbindlist(rubric_content)




# написать запрос, который возвращает ссылку на страницу по этому пути
page10_url <- 'https://ecsoc.hse.ru/rubric/26590246_page=2_sort=author.html'
page10 <- read_html(page10_url)

page10_content <- lapply(3:12, function(counter) {
  path <- paste('//*[@id="mainarea"]/div[2]/div[', counter, ']/small/a[2]', sep = '')
  url <- html_node(page10, xpath = path) %>% html_attr('href')
  
  # импортируем информацию о статье по ссылке
  article_content <- article_fetcher(article_url = url)
  return(article_content)
})
page10_content <- rbindlist(page10_content)
View(page10_content)





article_url <- 'https://ecsoc.hse.ru/2020-21-1/337414467.html'

res <- article_fetcher('https://ecsoc.hse.ru/2019-20-3/278706885.html')
View(res)




